#Written by Ryan Grimes 6/27/20 rygrim1@umbc.edu
#Version 3.d - refactored existing methods, added format class

x=1; y=2; z=3;

#-------------------------------------------------------------------------------------------------------------------------

#enter input dictionaries here

PbCO3_14 = {			
         1 : [ 'Pb', (0.25, 0.582, 0.246) ],     2 : [ 'Pb', (-0.25, 1.082, 0.254) ], 
         3 : [ 'Pb', (-0.25, -0.582, -0.246) ],  4 : [ 'Pb', (0.25, -0.082, 0.746) ], 
         5 : [ 'C', (0.25, 0.238, 0.088) ],      6 : [ 'C', (-0.25, 0.738, 0.412) ],
         7 : [ 'C', ( -0.25, -0.238, -0.088) ],  8 : [ 'C', (0.25, 0.262, 0.588) ],
         9 : [ 'O', (0.25, 0.086, 0.098) ],     10 : [ 'O', (-0.25, 0.586, 0.402) ],
        11 : [ 'O', (-0.25, -0.086, -0.098) ],  12 : [ 'O', (0.25, 0.414, 0.598) ],
        13 : [ 'O', (-0.033, 0.815, 0.409) ],   14 : [ 'O', (0.033, 1.315, 0.091) ], 
        15 : [ 'O', (0.033, -0.815, -0.409) ],  16 : [ 'O', (-0.033, -0.315, 0.909) ],
        17 : [ 'O', ( 0.467, 0.315, 0.091) ],   18 : [ 'O', (-0.467, 0.815, 0.409) ],
        19 : [ 'O', (-0.467, -0.315, -0.091) ], 20 : [ 'O', (0.467, 0.185, 0.591) ] 
           }

#TiO2 type Beta-PbO2 P42/mnm (#136) 4/mmm tP6 fa (Ryan)
PbO2_136 = {
         1 : [ 'Pb', (0.000, 0.000, 0.000) ], 2 :  [ 'Pb', (0.500, 0.500, 0.500) ], 
         3 : [ 'O', (0.308, 0.308, 0.000) ],  4 :  [ 'O', (-0.308, -0.308, 0.000) ], 
         5 : [ 'O', (0.192, 0.808, 0.500) ],  6 :  [ 'O', (0.808, 0.192, 0.500) ] 
           }

#Massicot type PbO Pbcm (#57) mmm oP8 d2 (Robert)
PbO_57 = {	
         1 : [ 'Pb', (0.231, 0.009, 0.250) ],  2 : [ 'Pb', (-0.231, -0.009, 0.750) ],  
         3 : [ 'Pb', (-0.231, 0.509, 0.250) ], 4 : [ 'Pb', (0.231, 0.491, 0.750) ], 
         5 : [ 'O', (0.139, 0.412, 0.250) ],   6 : [ 'O', (-0.139, -0.412, 0.750) ],
         7 : [ 'O', (-0.139, 0.912, 0.250) ],  8 : [ 'O', (0.139, 0.088, 0.750) ] 
         }

#Aragonite type PbCO3 Pnma (#62) oP20 dc3 (Robert)
PbCO3_62 = { 		
         1 : [ 'Pb', (0.245, 0.25, 0.583) ],     2 : [ 'Pb', (0.255, 0.75, 1.083) ], 
         3 : [ 'Pb', (-0.245, 0.75, -0.583) ],   4 : [ 'Pb', (0.745, 0.25, -0.083) ], 
         5 : [ 'C', (0.092, 0.25, 0.246) ],      6 : [ 'C', (0.408, 0.75, 0.746) ],
         7 : [ 'C', ( -0.092, 0.75, -0.246) ],   8 : [ 'C', (0.592, 0.25, 0.254) ], 
         9 : [ 'O', (0.096, 0.25, 0.093) ],     10 : [ 'O', (0.404, 0.75, 0.593) ], 
        11 : [ 'O', (-0.096, 0.75, -0.093) ],   12 : [ 'O', (0.596, 0.25, 0.407) ], 
        13 : [ 'O', (0.09, 0.035, 0.314) ],     14 : [ 'O', (0.41, -0.035, 0.814) ],
        15 : [ 'O', (-0.09, 0.535, -0.314) ],   16 : [ 'O', (0.59, 0.465, 0.186) ],
        17 : [ 'O', ( -0.09, -0.035, -0.314) ], 18 : [ 'O', (0.59, 0.035, 0.186) ],
        19 : [ 'O', (0.09, 0.465, 0.314) ],     20 : [ 'O', (0.41, 0.535, 0.814) ] 
           }

#Litharge type PbO P4/nmm (#129) 4/mmm tP4 ca (Josh)

PbO_129 = { 
         1 : [ 'O', (3/4,1/4,0) ],      2 : [ 'O', (1/4,3/4,0)], 
         3 : [ 'Pb', (1/4,1/4,0.233) ], 4 : ['Pb', (3/4,3/4,-0.233)]
          }

HPbPO4_7 = {
         1 : ['Pb',(0.500,0.699,0.233)],  2: ['Pb',(0.500,0.301,0.733)],
         3 : ['P',(-0.008,0.291,0.251)],  4: ['P',(-0.008,0.709,0.751)],
         5 : ['O',(0.238,0.423,0.367)],   6: ['O',(0.752,0.419,0.117)],
         7 : ['O',(0.752,0.581,0.617)],   8: ['O',(0.238,0.577,0.867)],
         9 : ['O',(0.123,0.151,0.067)],  10: ['O',(0.870,0.158,0.438)],
        11 : ['O',(0.870,0.842,0.938)],  12: ['O',(0.123,0.849,0.567)],
        13 : ['H',(0.006,-0.015,0.512)], 14: ['H',(0.006,0.015,0.512)]
           }

#END OF DICTIONARIES    
#*****************************************************USER-PARAMETERS*****************************************************

Name = ['PbCO3 #14'] #enter string corresponding to name of structure

output_key = ['Quantum'] #enter either 'Abinit' or 'Quantum' for desired output format of cleaved surface, default is Quantum Espresso format

#dimensions of supercell, 1x1x1 returns the primitive cell

X = 2
Y = 2
Z = 6

Sort_by = [z,y,x] #sorting priority for supercell coordinates - must be some permutation of x, y, and z

Charges  = {'Pb' : 2, 'O' : -2, 'C' : 4}

#shifts for x y and z coordinates of the cell - use to find inversion plane

x_shift = -0.25
y_shift = -0.25
z_shift = -0.50

STRUCTURE = PbCO3_14 #name of input dictionary for primitive cell atoms and coordinates

#****************************************************END-OF-PARAMETERS****************************************************

def Layer(prim_cell,Atoms,Layers):

	inversion = False; origin = False; k_inv = False; k_zero = False;
	Layered_Atoms = sorted(Atoms.items(), key = lambda x:(x[1][Sort_by[0]],x[1][Sort_by[1]],x[1][Sort_by[2]]))

	for i in range(len(Layered_Atoms)):[ Layers.update( { i + 1 : Layered_Atoms[i][1] } ) for i in range(len(Layered_Atoms)) ]

	for key, value in Layers.items(): 

		if (key != list(Layers)[-1]) and (Layers[key+1] == [ value[0],-value[1],-value[2],-value[3] ]):inversion = True; k_inv = key

		if value[1] == value[2] == value[3] == 0.0: origin = True; k_zero = key

	print('{:4}   {:2}     {:^12}   {:^12}   {:^12}\n'.format('','','x','y','z'))

	for key, value in Layers.items():

		if (inversion == True and key == k_inv) or (inversion == True and key == k_inv + 1):print(fmt.inversion.format(key,*value));

		if ((inversion == True) and (key != k_inv) and (key != k_inv + 1)) or inversion == False: print(fmt.main.format(key,*value))

	if inversion == True: print((color.BOLD + '\n***Possible inversion center found at {}-{}***\n' + color.END).format(k_inv,k_inv+1))

	if origin == True: print('\n***Possible inversion center found at {}**\n'.format(k_zero))

	return Layers

#----------------------------------------------------------------------------------------------------------------------------------

def Expand(prim_cell,Atoms,prim,X,Y,Z,x_shift,y_shift,z_shift):

	Atom = [ prim[1][0]/X, prim[1][1]/Y, prim[1][2]/Z ]

	L_X = [ [ Atom[0] + i/X, Atom[1], Atom[2] ] for i in range(X) ]; L_XY = [ [ Coord[0], Coord[1] + i/Y, Coord[2] ] for Coord in L_X for i in range(Y) ]

	L_XYZ = [ [ prim[0], round(Coord[0] + x_shift,9), round(Coord[1] + y_shift,9), round(Coord[2] + i/Z + z_shift, 9) ] for Coord in L_XY for i in range(Z) ]

	for i in range(len(L_XYZ)):

		if Atoms == { }: Atoms = { i : L_XYZ[i], **Atoms }

		else: Atoms =  { max( Atoms, key=int ) + 1 : L_XYZ[i], **Atoms };

	return Atoms

#----------------------------------------------------------------------------------------------------------------------------------

def Supercell(prim_cell,Atoms,Layers,X,Y,Z,x_shift,y_shift,z_shift):

	for i in range(len(prim_cell)): Atoms = Expand(prim_cell,Atoms,prim_cell[i+1],*Dim,*Shift)

	Layers = Layer(prim_cell,Atoms,Layers)
	print('\nEnter two numbers e.g. "10-20" corresponding to the atomic positions you would like to cleave a surface between')
	Surface = input('\n: ').split('-');print('\n');

	if Surface != [''] and (len(Surface) == 2): 

		[ Cleaved_Surface.update( { i + 1 : [ *Layers[i] ] } ) for i in range(int(Surface[0]),int(Surface[1])+1) ]

		for i in range(len(Cleaved_Surface)):

			if output_key[0] == 'Abinit':

				Atom_Sort = sorted(Cleaved_Surface.items(), key = lambda x: (x[1][0],x[1][Sort_by[0]]));
				print(fmt.abinit.format(Atom_Sort[i][1][1],Atom_Sort[i][1][2],Atom_Sort[i][1][3],Atom_Sort[i][1][0]))

			if output_key[0] == 'Quantum':

				Atom_Sort = sorted(Cleaved_Surface.items(), key = lambda x: (x[1][Sort_by[0]],x[1][Sort_by[1]],x[1][Sort_by[2]]))
				print(fmt.quantum.format(*Atom_Sort[i][1]))


#----------------------------------------------------------------------------------------------------------------------------------
Atoms = {}; Layers = {}; Cleaved_Surface = {}; Shift = [x_shift,y_shift,z_shift]; Dim = [X,Y,Z]; ast = '*'; Line = '\n'+ast*55+'\n'
key_map = {1: 'x', 2: 'y', 3: 'z' }; Sorter = [key_map.get(key, 'Null') for key in Sort_by]; cols = '{:4}:  {:2}  | {:12.9f} | {:12.9f} | {:12.9f}'

class color:
    CYAN = '\033[96m'
    BOLD = '\033[1m' 
    END = '\033[0m'
class fmt:
    main = cols;
    abinit = '  {:12.9f}  {:12.9f}  {:12.9f}  #{:3}';
    quantum = '{:2}  {:12.9f}  {:12.9f}  {:12.9f}';
    inversion = color.CYAN + color.BOLD + cols + color.END;
    
for dim in Dim:
	if (isinstance(dim,int) == False): print('\nCheck supercell dimension parameters.\n');raise SystemExit;
for shift in Shift: 
	if isinstance(shift,float) == False and shift != 0:  print('\nInvalid shift parameters.\n'); raise SystemExit
for sort in Sort_by:
	if Sort_by.count(sort) != 1 or len(Sort_by) != 3: print('Check Sort_by parameters.'); raise SystemExit;

if output_key[0] == '': output_key[0] = 'Quantum';
if (output_key[0] != 'Quantum') and (output_key[0] != 'Abinit'): print('Check output_key.'); raise SystemExit


if __name__ == "__main__":

	if X == Y == Z == 1: print('{}\n{} unaltered cell\n'.format(Line,Name[0]))
	else: print('{}\n{} X {} X {} {} supercell\n'.format(Line,X,Y,Z,Name[0]))

	print('x-shift: {} \ny-shift: {} \nz-shift: {} \n\nSorting priority: {}-{}-{}\n{}'.format(*Shift,*Sorter,Line))
	Supercell(STRUCTURE,Atoms,Layers,*Dim,*Shift)
