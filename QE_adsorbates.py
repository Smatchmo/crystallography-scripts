from sys import argv, exit

class fmt:

	col = '{:3}:  {:2}  | {:12.9f} | {:12.9f} | {:12.9f}'; H = '{:2}      {:12.9f}  {:12.9f}  {:12.9f}'

Redx = { 4.0 : [1.0795,24.304], 5.0 : [1.0636,30.380], 6.0 : [1.053,36.456], 3.0 : [1.053,36.456] }

Adsorbate = 'H'; Angstroms = 1.00; Atoms = {}; H = {}

try:
	file = argv[1]
except IndexError as e:
	print("Please enter a compatible .in file")
	exit()

if ".in" not in file:
	print("Please enter a compatible .in file")
	exit()

with open(file, 'r+') as f:
	for line in f:
		if ("nat" in line):
			index = int(line.split()[2].replace(",",""))
		if ("ATOMIC_POSITIONS" in line):
			for i in range(index):
				Sort = f.readline().split()
				Atoms.update( { i+1 : [ Sort[0],float(Sort[1]),float(Sort[2]),float(Sort[3]) ] }) 
		if ("celldm(3)" in line):
			Celldm = float(line.split()[2].replace(",",""))

print('\nParameters: celldm = {} angstroms = {} adsorbate = {}\n'.format(Celldm,Angstroms,Adsorbate))

for key,value in Atoms.items():

	print(fmt.col.format(key,*value))

Select = int(input('\nWhich position: '))

Shift = Redx[Celldm][0]/Redx[Celldm][1]

H1 = { index + 1 : [ Adsorbate,Atoms[Select][1],Atoms[Select][2],round(Atoms[Select][3] + Shift,9) ] } 

H.update(H1)

for key,value in Atoms.items():

	if value[3] == -1*Atoms[Select][3]:

		H2 = { index + 2 : [ Adsorbate,value[1],value[2],round(value[3] - Shift,10) ] }; H.update(H2)

H_Sort = sorted(H.items(), key = lambda x: (x[1][3])); print('\n')

for atom in H_Sort:

	print(fmt.H.format(*atom[1]))
